using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using Microsoft.Exchange.WebServices.Data;

namespace Alexa2017.Exchange
{
    public class SynergyExchangeService
    {
        private static readonly IList<string> RoomsList = new List<string>{"wroclaw", "puntacana", "saneskobar", "hongkong"};
        
        private static readonly AppSettingsReader ConfigReader = new AppSettingsReader();

        public ExchangeService GetRoomService(string roomName)
        {
            if (!RoomsList.Contains(roomName))
            {
                return null;
            }

            (string email, string password) account = GetRoomAccountData(roomName);

            return new ExchangeService(ExchangeVersion.Exchange2013)
            {
                Url = new Uri("https://outlook.office365.com/ews/exchange.asmx"),
                Credentials = new NetworkCredential(account.email, account.password)
            };
        }

        public IEnumerable<(ExchangeService service, string roomName)> GetAllRoomServices()
        {
            foreach (var room in RoomsList)
            {
                var service = this.GetRoomService(room);
                if (service != null)
                {
                    yield return (service, room);
                }
            }
        }

        (string email, string password) GetRoomAccountData(string room)
        {
            object value = ConfigReader.GetValue(room, typeof(string));
            string[] accountData = value.ToString().Split('#');

            return (email: accountData[0], password: accountData[1]);
        }
    }
}