﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;

namespace Alexa2017.Exchange
{
    public class AppointmentsProvider
    {
        private readonly ExchangeService service;

        public AppointmentsProvider(ExchangeService service)
        {
            this.service = service;
        }

        public IEnumerable<Appointment> GetRoomAppointments(DateTime startDate, DateTime endDate)
        {
            CalendarFolder calendar = CalendarFolder.Bind(service, WellKnownFolderName.Calendar, new PropertySet());
            CalendarView cView = new CalendarView(startDate, endDate)
            {
                PropertySet = new PropertySet(ItemSchema.Subject, AppointmentSchema.Start, AppointmentSchema.End,
                    AppointmentSchema.AppointmentState, AppointmentSchema.StartTimeZone)
            };
            
            
            return calendar.FindAppointments(cView);
        }
    }
}