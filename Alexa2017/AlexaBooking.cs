using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using System;
using Alexa2017.Booking;

namespace Alexa2017
{
    public static class AlexaBooking
    {
        [FunctionName("AlexaBooking")]
        public static async Task<HttpResponseMessage> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequestMessage req,
            TraceWriter log)
        {
            dynamic data = await req.Content.ReadAsAsync<object>();
            dynamic slots = data.request.intent?.slots;

            DateTime? bookingDate = slots?.bookingDate?.value;
            DateTime? bookingTime = slots?.bookingTime?.value;
            int durationMinutes = slots?.durationMinutes?.value ?? 0;
            string room = slots?.room?.value;
            room = string.IsNullOrEmpty(room) ? room : room.Replace(" ", "").ToLower();

            BookingRequest request = new BookingRequest(room, bookingDate, bookingTime, durationMinutes);

            string intentName = data.request.intent.name;

            IBookingManager manager = GetIntentManager(intentName);
            bool isCompletedRequest = manager.IsCompleted(request);

            if (!isCompletedRequest)
            {
                return req.CreateResponse(HttpStatusCode.OK, new
                    {
                        version = "1.0",
                        sessionAttributes = new { },
                        response = new
                        {
                            directives = new[]
                            {
                                new
                                {
                                    type = "Dialog.Delegate"
                                }
                            },
                            shouldEndSession = false
                        }
                    }
                );
            }

            string response = manager.Run(request);

            return req.CreateResponse(HttpStatusCode.OK, new
            {
                version = "1.0",
                sessionAttributes = new { },
                response = new
                {
                    outputSpeech = new
                    {
                        type = "PlainText",
                        text = response
                    },
                    shouldEndSession = true
                }
            });
        }

        private static IBookingManager GetIntentManager(string intent)
        {
            switch (intent)
            {
                case "roomchecking":
                    return new CheckingManager();
                case "bookingextending":
                    return new ExtendingManager();
                case "roombooking":
                    return new BookingManager();
                case "roomseeking":
                    return new RoomSeekingManager();
                default:
                    return new DefaultaManager();
            }
        }
    }
}