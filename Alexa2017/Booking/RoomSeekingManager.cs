﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alexa2017.Exchange;
using Microsoft.Exchange.WebServices.Data;

namespace Alexa2017.Booking
{
    public class RoomSeekingManager : IBookingManager
    {
        public string Run(BookingRequest request)
        {
            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            IEnumerable<(ExchangeService service, string roomName)> services = synergyExchangeService.GetAllRoomServices();

            var roomServices = new List<(ExchangeService exchangeService, string roomName)>();
            DateTime now = DateTime.Now;

            foreach (var service in services)
            {
                AppointmentsProvider provider = new AppointmentsProvider(service.service);
                Appointment collision = provider.GetRoomAppointments(now, now).FirstOrDefault();

                if (collision == null)
                {
                    roomServices.Add(service);
                }
            }

            if (!roomServices.Any())
            {
                return "There are no free rooms now";
            }

            StringBuilder builder = new StringBuilder($"There are following rooms available:");

            foreach ((ExchangeService service, string roomName) service in roomServices)
            {
                AppointmentsProvider provider = new AppointmentsProvider(service.Item1);

                Appointment next = provider.GetRoomAppointments(now, now.Date.AddDays(1)).OrderBy(i => i.Start)
                    .FirstOrDefault();

                string availableFor;
                if (next != null)
                {
                    int minutes = (next.Start - DateTime.Now).Minutes % 60;
                    int hours = (next.Start - DateTime.Now).Hours;
                    availableFor = hours > 0
                        ? $"for {hours} hours and {minutes} minutes."
                        : $"for {minutes} minutes.";
                }
                else
                {
                    availableFor = " for the rest of the day.";
                }

                builder.Append($" {service.roomName} {availableFor}");
            }

            return builder.ToString();
        }

        public bool IsCompleted(BookingRequest request)
        {
            return true;
        }
    }
}