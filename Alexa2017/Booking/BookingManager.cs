﻿using System;
using System.Collections.Generic;
using System.Linq;
using Alexa2017.Exchange;
using Microsoft.Exchange.WebServices.Data;

namespace Alexa2017.Booking
{
    public class BookingManager : IBookingManager
    {
        public string Run(BookingRequest request)
        {
            return !string.IsNullOrEmpty(request.Room) ? this.BookRoom(request) : this.FindRoom(request);
        }

        private string FindRoom(BookingRequest request)
        {
            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            IEnumerable<(ExchangeService service, string roomName)> services = synergyExchangeService.GetAllRoomServices();

            foreach (var service in services)
            {
                AppointmentsProvider provider = new AppointmentsProvider(service.service);
                Appointment collision = provider.GetRoomAppointments(request.BookingDateTime, request.BookingEndDatetime).FirstOrDefault();

                if (collision == null)
                {
                    return this.MakeBooking(service.Item1, service.roomName, request.BookingDateTime, request.BookingEndDatetime);
                }
            }

            return "There is no room available for your booking criteria!";
        }

        private string BookRoom(BookingRequest request)
        {
            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            ExchangeService service = synergyExchangeService.GetRoomService(request.Room);

            if (service == null)
            {
                return $"Sorry, I don't know this {request.Room} room";
            }

            AppointmentsProvider provider = new AppointmentsProvider(service);

            Appointment collision = provider.GetRoomAppointments(request.BookingDateTime, request.BookingEndDatetime).FirstOrDefault();

            if (collision != null)
            {
                return $"Sorry, there is a collision with {collision.Subject} that starts at {collision.Start:hh:mm:ss tt} and ends at {collision.End:hh:mm:ss tt}";
            }

           return this.MakeBooking(service, request.Room, request.BookingDateTime, request.BookingEndDatetime);
        }

        private string MakeBooking(ExchangeService service, string room, DateTime startDate, DateTime endDate)
        {
            var meeting = new Appointment(service)
            {
                Subject = "Alexa Booking",
                Body = "Alexa Automatic",
                Start = startDate,
                End = endDate
            };

            meeting.Save(SendInvitationsMode.SendToAllAndSaveCopy);

            return $"OK. Your reservation in {room} starts on {startDate:M} at {startDate:hh:mm:ss tt} and ends at {endDate:hh:mm:ss tt}";
        }

        public bool IsCompleted(BookingRequest request)
        {
            return request.DurationMinutes > 0 && request.BookingDate.HasValue && request.BookingTime.HasValue;
        }
    }
}
