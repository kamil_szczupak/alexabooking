﻿namespace Alexa2017.Booking
{
    interface IBookingManager
    {
        string Run(BookingRequest request);

        bool IsCompleted(BookingRequest request);
    }
}