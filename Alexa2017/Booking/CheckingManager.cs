using System;
using System.Linq;
using Alexa2017.Exchange;
using Microsoft.Exchange.WebServices.Data;

namespace Alexa2017.Booking
{
    public class CheckingManager : IBookingManager
    {
        public string Run(BookingRequest request)
        {
            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            ExchangeService service = synergyExchangeService.GetRoomService(request.Room);

            if (service == null)
            {
                return $"Sorry, I don't know this {request.Room} room";
            }

            AppointmentsProvider provider = new AppointmentsProvider(service);

            DateTime now = DateTime.Now;
            var current = provider.GetRoomAppointments(now, now).FirstOrDefault();

            if (current != null)
            {
                return $"There is currently {current.Subject} that ends at {current.End}";
            }
            
            Appointment next = provider.GetRoomAppointments(now, DateTime.Now.Date.AddDays(1)).FirstOrDefault();

            return next == null ? "Yes, it's available for the rest of the day!" : $"Yes, it's available till {next.Start:hh:mm:ss tt} when {next.Subject} begins";
        }

        public bool IsCompleted(BookingRequest request)
        {
            return !string.IsNullOrEmpty(request.Room);
        }
    }
}