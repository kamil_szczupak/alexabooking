﻿using System;
using System.Linq;
using Alexa2017.Exchange;
using Microsoft.Exchange.WebServices.Data;

namespace Alexa2017.Booking
{
    public class ExtendingManager : IBookingManager
    {
        public string Run(BookingRequest request)
        {
            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            ExchangeService service = synergyExchangeService.GetRoomService(request.Room);

            if(service == null)
            {
                return $"Sorry, I don't know this {request.Room} room";
            }

            AppointmentsProvider provider = new AppointmentsProvider(service);

            DateTime now = DateTime.Now;
            Appointment current = provider.GetRoomAppointments(now, now).FirstOrDefault();

            if (current == null)
            {
                return "There is no running reservation in this room. Book the room first!";
            }
            
            DateTime newStart = current.End;
            DateTime newEnd = current.End.AddMinutes(request.DurationMinutes);

            Appointment collision = provider.GetRoomAppointments(newStart, newEnd).FirstOrDefault(i => i.Id.UniqueId != current.Id.UniqueId);

            if (collision != null)
            {
                return $"Sorry, there is a collision with {collision.Subject} that starts at {collision.Start:hh:mm:ss tt}";
            }
            
            var meeting = new Appointment(service)
            {
                Subject = $"{current.Subject} - Alexa Booking",
                Body = "Alexa Automatic",
                Start = newStart,
                End = newEnd
            };
            
            meeting.Save(SendInvitationsMode.SendToAllAndSaveCopy);

            return $"Done. Your reservation ends at {newEnd:hh:mm:ss tt}";
        }

        public bool IsCompleted(BookingRequest request)
        {
            if (string.IsNullOrEmpty(request.Room))
            {
                return false;
            }

            SynergyExchangeService synergyExchangeService = new SynergyExchangeService();
            ExchangeService service = synergyExchangeService.GetRoomService(request.Room);

            if (service == null)
            {
                return true;
            }

            AppointmentsProvider provider = new AppointmentsProvider(service);

            DateTime now = DateTime.Now;
            Appointment current = provider.GetRoomAppointments(now, now).FirstOrDefault();

            return current == null || request.DurationMinutes > 0;
        }
    }
}
