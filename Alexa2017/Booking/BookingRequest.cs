using System;

namespace Alexa2017.Booking
{
    public class BookingRequest
    {
        public BookingRequest(string room, DateTime? bookingDate, DateTime? bookingTime, int durationMinutes)
        {
            this.Room = room;
            this.BookingDate = bookingDate;
            this.BookingTime = bookingTime;
            this.DurationMinutes = durationMinutes;
        }

        public int DurationMinutes { get; set; }

        public DateTime? BookingDate { get; set; }

        public DateTime? BookingTime { get; set; }

        public string Room { get; set; }

        public DateTime BookingDateTime => this.BookingDate.HasValue && this.BookingTime.HasValue
            ? new DateTime(BookingDate.Value.Year, BookingDate.Value.Month,
                BookingDate.Value.Day, BookingTime.Value.Hour, BookingTime.Value.Minute, BookingTime.Value.Second)
            : DateTime.Now;

        public DateTime BookingEndDatetime => this.BookingDateTime.AddMinutes(this.DurationMinutes);
    }
}